﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using Windows.Storage;
using System.Threading.Tasks;
using MusicLibrary;
using Windows.Storage.Pickers;
using System.Threading;
using System.Xml.Serialization;
using System.Xml;
using System.Runtime.Serialization;
using System.IO;

using MusicLibrary.Factory;
using MusicLibrary.Factory.Progress;
using System.Diagnostics;

namespace MusicLibraryFactoryUnitTests
{
    [TestClass]
    public class LibraryScanning
    {
        [TestMethod]
        public void MusicLibraryScanning()
        {
            Library musicLibrary = ScanMusicLibrary();
            //System.Threading.Tasks.Task.Delay(1000).Wait();
            SerialiseMusicLibrary(musicLibrary);
        }

        private Library ScanMusicLibrary()
        {
            StorageFolder musicFolder = KnownFolders.MusicLibrary;

            NativeLibraryScanner libraryScanner = new NativeLibraryScanner();
            Progress<ScanProgress> scanProgress = new Progress<ScanProgress>();
            CancellationTokenSource cancelScanTS = new CancellationTokenSource();

            Task<Library> musicLibraryTask = libraryScanner.CreateLibrary(
                musicFolder, scanProgress, cancelScanTS.Token);

            Library musicLibrary = musicLibraryTask.Result;
            Assert.IsInstanceOfType(musicLibrary, typeof(Library));
            //Assert.AreEqual(musicLibrary.Tracks.Count, 1381);
            return musicLibrary;
        }

        private void SerialiseMusicLibrary(Library musicLibrary)
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(Library));
            
            MemoryStream libraryStream = new MemoryStream();
            serializer.WriteObject(libraryStream, musicLibrary);

            Assert.AreNotEqual(libraryStream.Length, 0);

            Debug.WriteLine(
                "Music Library with {0} Tracks, {1} Album Artists, {2} Albums, {3} Genres resolved to {4}KB.",
                musicLibrary.Tracks.Count,
                musicLibrary.AlbumArtists.Count,
                musicLibrary.Albums.Count,
                musicLibrary.Genres.Count,
                (double)Math.Round((double)libraryStream.Length / (double)1024 * 10) / (double)10
                );
        }


    }
}
