﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Windows.Storage;

namespace MusicLibrary.LibraryItems
{
    [DataContract(Namespace = "https://standard.kashmaniac.com.au/2015/music-library/library-items/track", IsReference = true)]
	public class Track
    {
        [DataMember(Name="Number")]
        public uint Number { get; set; }
        [DataMember(Name = "Title")]
        public String Title { get; set; }
        [DataMember(Name = "Duration")]
		public TimeSpan Duration { get; set; }
        [DataMember(Name = "Album")]
		public Album Album { get; set; }
        [DataMember(Name = "AlbumArtist")]
		public AlbumArtist AlbumArtist { get; set; }
        [DataMember(Name = "Artists")]
		public List<Artist> Artists { get; set; }
        
        [IgnoreDataMember]
		public StorageFile MusicStorageFile { get; set; }

        [DataMember(Name="IsAvailable")]
        public bool IsAvailable { get; set; }

        [DataMember(Name="Year")]
        public uint Year { get; set; }

        public Track()
        {
            Artists = new List<Artist>();
        }
	}
}