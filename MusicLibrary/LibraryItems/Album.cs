﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MusicLibrary.LibraryItems
{
    [DataContract(Namespace = "https://standard.kashmaniac.com.au/2015/music-library/library-items/album", IsReference=true)]
	public class Album
    {
        [DataMember(Name="Name")]
        public string Name { get; set; }

        [DataMember(Name = "Year")]
        public uint Year { get; set; }
        
        [DataMember(Name = "AlbumArtist")]
		public AlbumArtist Artist { get; set; }
        
        [DataMember(Name = "Tracks")]
		public List<Track> Tracks { get; set; }
        
        [DataMember(Name = "Genres")]
        public HashSet<Genre> Genres { get; set; }

        public Album()
        {
            Tracks = new List<Track>();
            Genres = new HashSet<Genre>();
        }

        [IgnoreDataMember]
        public byte[] AlbumArt;

        [DataMember(Name="AlbumArtCacheIndex")]
        public int[] AlbumArtCacheIndex = { 0, 0 };
    }
}
