﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace MusicLibrary.LibraryItems
{
    [DataContract(Namespace = "https://standard.kashmaniac.com.au/2015/music-library/library-items/genre", IsReference = true)]
	public class Genre
	{
        [DataMember(Name="Name")]
		public string Name { get; set; }
        
        [DataMember(Name="Albums")]
		public Dictionary<string, Album> Albums { get; set; }

        public Genre()
        {
            Albums = new Dictionary<string, Album>();
        }
	}
}

