﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MusicLibrary.LibraryItems
{
    [DataContract(Namespace = "https://standard.kashmaniac.com.au/2015/music-library/library-items/artist", IsReference = true)]
	public class Artist
    {
        [DataMember(Name="Name")]
        public String Name { get; set; }
    }
}
