﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MusicLibrary.LibraryItems
{
    [DataContract(Namespace = "https://standard.kashmaniac.com.au/2015/music-library/library-items/album-artist", IsReference = true)]
	public class AlbumArtist : Artist
    {
        [DataMember(Name="Albums")]
		public HashSet<Album> Albums;

        public AlbumArtist()
        {
            Albums = new HashSet<Album>();
        }
    }
}
