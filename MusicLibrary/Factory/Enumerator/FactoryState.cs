﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicLibrary.Factory.Enumerator
{
    public enum FactoryState
    {
        Idle,
        Retreiving,
        Processing,
        Completed,
        Cancelled
    }
}
