﻿using MusicLibrary.Factory.Enumerator;
using MusicLibrary.Factory.Exception;
using MusicLibrary.Factory.Intermediate;
using MusicLibrary.Factory.Progress;
using MusicLibrary.LibraryItems;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.Storage.Search;

namespace MusicLibrary.Factory
{
    /// <summary>
    /// Designed for use with the Windows Music library, where library files
    /// are selected by the user and consolidated into a virtual folder (or
    /// "Library" in Windows terms.
    /// 
    /// This scanner is the default for use with the MusicLibrary app and
    /// prefers performance over extensibility, with the assumption that file
    /// access is relatively quick (read: local).
    /// </summary>
    public class NativeLibraryScanner
    {
        public readonly static string[] ValidFileTypes =
            { ".m4a", ".aac", ".wma", ".mp3" };
        
        public FactoryState CurrentState { get; protected set; }
        public int Processed
        {
            get
            {
                return Total - musicStorageFiles.Count;
            }
            set
            {
                Processed = 0;
            }
        }
        public int Total { get; protected set; }

        const int ProcessQueueSize = 20;
        ConcurrentBag<StorageFile> musicStorageFiles;
        BlockingCollection<StorageFile> processMusicFileList;
        ConcurrentBag<Task> scanTasks;

        Library musicLibrary;

        /// <summary>
        /// Constructor for our library scanner, which sets up the necessary
        /// variables for keeping track of a scan.
        /// </summary>
        public NativeLibraryScanner()
        {
            CurrentState = FactoryState.Idle;
            scanTasks = new ConcurrentBag<Task>();
            musicLibrary = new Library();

            Total = -1;
        }

        /// <summary>
        /// Performs the scan of the nominated storage folder. This is a long-
        /// running asnychronous task with progress updates that are issued for
        /// each phase change during the scan (a more granular progress 
        /// indicator is available in the public Processed and Total
        /// properties.)
        /// The method also takes a cancellation token so that scanning may be
        /// halted on request.
        /// </summary>
        /// <param name="musicFolder">
        /// The storage folder to scan for files.
        /// </param>
        /// <param name="factoryStateChange">
        /// Updates the caller with notifications of major state changes to the
        /// scanning process.
        /// </param>
        /// <param name="scanCancellationToken">
        /// A token that may be used to halt the scan prematurely.
        /// </param>
        /// <returns>Returns a furnished Library object that represents each 
        /// of the files found in the specified folder.</returns>
        public async Task<Library> CreateLibrary(StorageFolder musicFolder,
            IProgress<ScanProgress> factoryStateChange,
            CancellationToken scanCancellationToken)
        {
            // Step 1 of 2: Get a list of files
            CurrentState = FactoryState.Retreiving;
            try
            {
                musicStorageFiles = GetFileList(
                    musicFolder, scanCancellationToken).Result;
            }
            catch (OperationCanceledException ocs)
            {
                CurrentState = FactoryState.Cancelled;
                throw new OperationCanceledException(
                    "Music files not fetched.", ocs);
            }
            catch (NoMusicFilesFoundException nme)
            {
                throw new NoMusicFilesFoundException("GetFileList failed to "
                + "find any files in the supplied folder (see InnerException "
                + "for path).", nme); 
            }

            scanCancellationToken.ThrowIfCancellationRequested();

            processMusicFileList = new BlockingCollection<StorageFile>(
                ProcessQueueSize);

            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                Task producer = new Task(async () =>
                    {
                        await CreateTrack_Producer(musicStorageFiles,
                            processMusicFileList,
                            scanCancellationToken);
                    });
                producer.Start();

                for (var i = 0; i < 16; i++)
                    scanTasks.Add(new Task(
                        async () =>
                        {
                            try
                            {
                                await CreateTrack_Consumer(
                                    processMusicFileList,
                                    scanCancellationToken);
                            }
                            catch (OperationCanceledException oce) { }
                        }, TaskCreationOptions.LongRunning)
                    );

                Parallel.ForEach<Task>(scanTasks, (Task scanTask) 
                    => scanTask.Start());

                scanTasks.Add(producer);
                Task.WaitAll(scanTasks.ToArray(), scanCancellationToken);
                sw.Stop();
                Debug.WriteLine(
                    "Library processing completed in {0} files in {1} seconds.",
                    musicLibrary.Tracks.Count,
                    (double)sw.ElapsedMilliseconds / (double)1000);
            } catch (OperationCanceledException oce)
            {
                CurrentState = FactoryState.Cancelled;
                return null;
            }
            
            
            return musicLibrary;
        }

        /// <summary>
        /// Moves files from the list of files identified in the source folder
        /// to the processing list. This producer simply queues in a way that
        /// leverages a bounded blocking collection to avoid going too fast.
        /// </summary>
        /// <param name="musicFile"></param>
        async Task CreateTrack_Producer(
            ConcurrentBag<StorageFile> allMusicStorageFiles,
            BlockingCollection<StorageFile> processingList,
            CancellationToken scanCancellationToken)
        {
            CurrentState = FactoryState.Processing;

            StorageFile nextBagStorageFile;
            try
            {
                // Seeing as we're working with a blocking collection we can
                // implement the producer/consumer pattern to process files.
                do
                {
                    //Debug.WriteLine("CreateTrack_Producer: Processing file {0} of {1}", allMusicStorageFiles.Count, Total);
                    
                    allMusicStorageFiles.TryTake(out nextBagStorageFile);
                    if (nextBagStorageFile != null)
                    {
                        // Blocks until the collection has space.
                        processingList.Add(nextBagStorageFile);
                    }
                    scanCancellationToken.ThrowIfCancellationRequested();
                } while (nextBagStorageFile != null);
                
                // Not reached until bag is emptied.
                processMusicFileList.CompleteAdding();
            }
            catch (OperationCanceledException oce)
            {
                throw new OperationCanceledException(
                    String.Format("Interrupted file processing on file {0} of "
                    + "{1}.", Processed, Total), oce);
            }
            catch (InvalidOperationException ioe)
            {
                throw;
            }

        }

        /// <summary>
        /// Takes a file reference from the collection and 
        /// </summary>
        /// <param name="musicFiles">The blocking collection that should be
        /// consumed with each step.</param>
        /// <param name="scanCancellationToken">Allows the process to be interrupted
        /// if needed.</param>
        /// <returns>An empty task (this method runs synchronously).</returns>
        async Task CreateTrack_Consumer(
            BlockingCollection<StorageFile> musicFiles,
            CancellationToken scanCancellationToken)
        {
            try
            {
                do
                {
                    //Debug.WriteLine("CreateTrack_Consumer: Processing file...");
                    StorageFile musicFile = musicFiles.Take(scanCancellationToken);
                    LocalTrackFile musicLocalTrackFile = 
                        CreateLocalTrackFile(musicFile).Result;

                    Track musicTrack;
                    lock (musicLibrary)
                    {
                        musicTrack = musicLibrary.AddTrack(musicLocalTrackFile);
                    }
                    var thumb = await musicFile.GetThumbnailAsync(ThumbnailMode.MusicView, (uint)500, ThumbnailOptions.ResizeThumbnail);

                    scanCancellationToken.ThrowIfCancellationRequested();
                } while (!musicFiles.IsCompleted);
            }
            catch (OperationCanceledException oce)
            {
                // Manual cancellation
                throw new OperationCanceledException(String.Format("Track "
                        + "creation cancelled on file #{0}", 
                        musicStorageFiles.Count));
            }
            catch (InvalidOperationException ioe)
            {
                // IsCompleted set by the producer(s)
                Debug.WriteLine("CreateTrack_Consumer: Exception with {0} tracks pending processing queue.", musicStorageFiles.Count);
            }

            Debug.WriteLine("CreateTrack_Consumer: Exiting with {0} tracks pending processing queue.", musicStorageFiles.Count);
        }

        /// <summary>
        /// Creates a LocalTrackFile object from a music file so that the meta-
        /// data can be used to correctly place the music file in the library.
        /// </summary>
        /// <param name="musicFile">The file that will be added to the library.
        /// </param>
        /// <returns>A funrished LocalTrackFile object.</returns>
        async Task<LocalTrackFile> CreateLocalTrackFile(StorageFile musicFile)
        {
            var musicProperties
                = await musicFile.Properties.GetMusicPropertiesAsync();

            LocalTrackFile localTrackFile = new LocalTrackFile()
            {
                Number = musicProperties.TrackNumber,
                Title = musicProperties.Title,
                Duration = musicProperties.Duration,
                Album = musicProperties.Album,
                AlbumArtist = musicProperties.AlbumArtist,
                MusicStorageFile = musicFile,
                Genres = musicProperties.Genre
            };

            return localTrackFile;
        }

        /// <summary>
        /// Retreives a list of files indexed by the operating system within
        /// the specified storage folder.
        /// </summary>
        /// <param name="musicFolder">
        /// The storage folder to scan for files.
        /// </param>
        /// <param name="cancelFileLookup">
        /// A token that may be used to cancel this request.
        /// </param>
        /// <returns>Returns a list of files that were found within the 
        /// specified folder.</returns>
        async Task<ConcurrentBag<StorageFile>> GetFileList(
            StorageFolder musicFolder, CancellationToken cancelFileLookup)
        {
            Debug.WriteLine("GetFileList: Fetching music files in {0}",
                musicFolder.Path);
            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            var allFilesTask = musicFolder.GetFilesAsync(
                CommonFileQuery.OrderByName).AsTask();

            try
            {
                while (!allFilesTask.IsCompleted)
                    cancelFileLookup.ThrowIfCancellationRequested();
            } catch (OperationCanceledException oce)
            {
                CurrentState = FactoryState.Cancelled;
                throw new OperationCanceledException(
                    "Cancelled fetching files from the OS");
            }

            var allFiles = allFilesTask.Result;

            stopwatch.Stop();
            Debug.WriteLine("GetFileList: Found {0} files in {1} milliseconds",
                allFiles.Count, stopwatch.ElapsedMilliseconds);

            ConcurrentBag<StorageFile> musicFiles =
                new ConcurrentBag<StorageFile>();

            Debug.WriteLine("GetFileList: Removing non-music files from "
            + "collection");
            
            stopwatch.Reset();
            stopwatch.Start();

            Parallel.ForEach(allFiles, (StorageFile file) =>
            {
                if (ValidFileTypes.Contains(file.FileType))
                    musicFiles.Add(file);

                cancelFileLookup.ThrowIfCancellationRequested();
            });

            stopwatch.Stop();
            Debug.WriteLine("GetFileList: Removed {0} non-music files in "
             + "{1} milliseconds", allFiles.Count - musicFiles.Count,
            stopwatch.ElapsedMilliseconds);

            Total = musicFiles.Count;

            if (musicFiles.Count == 0)
                throw new NoMusicFilesFoundException(
                    String.Format("There were no music files found in {0}",
                    musicFolder.Path)
                    );

            return musicFiles;
        }
    }
}
