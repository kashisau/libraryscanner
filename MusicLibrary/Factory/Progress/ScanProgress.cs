﻿using MusicLibrary.Factory.Enumerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicLibrary.Factory.Progress
{
    public class ScanProgress
    {
        public FactoryState Old { get; set; }
        public FactoryState New { get; set; }
    }
}
