﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace MusicLibrary.Writers.ArtCache
{
    /// <summary>
    /// This writer may be used to extract album art from music files and save
    /// it into a single art cache file.
    /// </summary>
    public class NativeArtCacheWriter
    {
        public NativeArtCacheWriter()
        {
            
        }   
    }
}
