﻿using System;
using System.Collections.Generic;
using MusicLibrary.LibraryItems;
using System.Runtime.Serialization;
using MusicLibrary.Factory.Intermediate;
using System.Threading.Tasks;

namespace MusicLibrary
{
    [DataContract(Namespace = "https://standard.kashmaniac.com.au/2015/music-library",IsReference=true)]
    public class Library
    {
        [DataMember(Name = "Albums")] 
        public Dictionary<string, Album> Albums { get; private set; }
        [DataMember(Name = "AlbumArtists")]
        public Dictionary<string, AlbumArtist> AlbumArtists { get; private set; }
        [DataMember(Name = "Genres")]
        public Dictionary<string, Genre> Genres { get; private set; }
        [DataMember(Name = "Tracks")]
        public HashSet<Track> Tracks { get; private set; }

        public Library()
        {
            Albums = new Dictionary<string, Album>();
            AlbumArtists = new Dictionary<string, AlbumArtist>();
            Genres = new Dictionary<string, Genre>();
            Tracks = new HashSet<Track>();
        }

        /// <summary>
        /// Adds a trackfile to the library, creating the required album,
        /// artist and genre where necessary. This method will return the Track
        /// object furnished with the elements of the Library.
        /// </summary>
        /// <param name="trackFile">The trackfile to add to the library with 
        /// the metadata for album, artist and genre in place.</param>
        /// <returns></returns>
        public Track AddTrack(LocalTrackFile trackFile)
        {
            string albumName = trackFile.Album;
            string artistName = trackFile.AlbumArtist;

            List<Genre> trackGenres = new List<Genre>();

            lock (Genres) lock (AlbumArtists) lock (Albums)
            {
                // 1 of 3: Check the genre
                foreach (string genreName in trackFile.Genres)
                {
                    trackGenres.Add(LibraryGenre(genreName));
                }

                // 2 of 3: Check the album
                Album trackAlbum = LibraryAlbum(albumName);

                Parallel.ForEach(trackGenres, (trackGenre) =>    
                {
                    if (!trackGenre.Albums.ContainsKey(albumName))
                        trackGenre.Albums.Add(albumName, trackAlbum);

                    if (!trackAlbum.Genres.Contains(trackGenre))
                        trackAlbum.Genres.Add(trackGenre);
                });
                AlbumArtist trackAlbumArtist = LibraryAlbumArtist(artistName);
                if (trackAlbumArtist.Albums.Contains(trackAlbum))
                    trackAlbumArtist.Albums.Add(trackAlbum);
                
                trackAlbum.Artist = trackAlbumArtist;
                // 2 of 3: Check the album artist
                Track track = new Track()
                {
                    Title = trackFile.Title,
                    Album = trackAlbum,
                    AlbumArtist = trackAlbumArtist,
                    Duration = trackFile.Duration,
                    MusicStorageFile = trackFile.MusicStorageFile,
                    Number = trackFile.Number,
                    Year = trackFile.Year
                };
                
                trackAlbum.Tracks.Add(track);
                Tracks.Add(track);
                return track;
            }
            throw new Exception("Error getting lock on musicItems.");
        }

        /// <summary>
        /// Retreives an AlbumArtist from the library or creates a new one,
        /// adding it to the library before returning it for manipulation;
        /// </summary>
        /// <param name="albumName">The name of the album artist.</param>
        /// <returns>An AlbumArtist that is in the library.</returns>
        private AlbumArtist LibraryAlbumArtist(string artistName)
        {
            if (AlbumArtists.ContainsKey(artistName))
                return AlbumArtists[artistName];

            AlbumArtist targetAlbumArtist = new AlbumArtist()
            {
                Name = artistName
            };
            AlbumArtists.Add(artistName, targetAlbumArtist);

            return targetAlbumArtist;
        }

        /// <summary>
        /// Retreives a Genre from the library or creates a new Genre, adding
        /// it to the library before returning it for manipulation.
        /// </summary>
        /// <param name="genreName">The name of the genre.</param>
        /// <returns>A Genre that is in the library.</returns>
        protected Genre LibraryGenre(string genreName)
        {
            if (Genres.ContainsKey(genreName))
                return Genres[genreName];

            Genre targetGenre = new Genre()
            {
                Name = genreName
            };

            Genres.Add(genreName, targetGenre);
            return targetGenre;
        }

        /// <summary>
        /// Retreives an Album from the library or creates a new Album, adding
        /// it to the library before returning it for manipulation;
        /// </summary>
        /// <param name="albumName">The name of the album.</param>
        /// <returns>An Album that is in the library.</returns>
        protected Album LibraryAlbum(string albumName)
        {
            if (Albums.ContainsKey(albumName))
                return Albums[albumName];

            Album targetAlbum = new Album()
            {
                Name = albumName
            };

            Albums.Add(albumName, targetAlbum);
            return targetAlbum;
        }
    }
}

