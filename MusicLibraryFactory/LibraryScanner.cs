﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Search;
using MusicLibrary;
using MusicLibrary.LibraryItems;
using Windows.Storage.FileProperties;
using MusicLibraryBuilder.FileConcepts;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Diagnostics;

namespace MusicLibraryFactory
{
    /// <summary>
    /// This class may be used to build a music library on a Universal app
    /// from the user's music library. The resulting library is an instance of
    /// the MusicLibrary.Library class furnished with the contents of the
    /// specified folder.
    /// </summary>
    public class LibraryScanner
    {
        public enum ScanStates
        {
            Idle,
            CheckingFolderFiles,
            ListFiles,
            ReadingMetaData,
            Cancelled
        }

        public const uint ThumbSize = 300;

        public delegate bool PlatformFileQualifier(StorageFile musicFile);
        public ScanStates CurrentScanState;
        public int FilesProcessed = 0;
        public int TotalFiles = -1;
        public string LastSongAdded;

        protected ConcurrentQueue<StorageFile> FileList;
        protected Library MusicLibrary;

        public LibraryScanner()
        {
            FileList = new ConcurrentQueue<StorageFile>();
            MusicLibrary = new Library();
            CurrentScanState = ScanStates.Idle;
        }

        /// <summary>
        /// Scans an assigned folder for music files and organises these files
        /// into a Library object. This method will spin up a number of
        /// background tasks to leverage multicore machines.
        /// </summary>
        /// <param name="musicFolder">
        /// The location of the music library to be
        /// scanned.
        /// </param>
        /// <returns>A fully-stocked music library.</returns>
        public async Task<Library> ScanFolder(StorageFolder musicFolder,
            CancellationToken scanCancellationToken)
        {
            CurrentScanState = ScanStates.CheckingFolderFiles;

            // Get a list of all the files that we're going to scan.
            ConcurrentQueue<StorageFile> FileList = await GetMusicFiles(musicFolder);
            TotalFiles = FileList.Count;

            CurrentScanState = ScanStates.ReadingMetaData;

            // Break up the file list into more managable chunks.
            try
            {
                ParallelOptions forEachOptions = new ParallelOptions()
                {
                    CancellationToken = scanCancellationToken
                };

                Parallel.ForEach(FileList, forEachOptions, musicStorageFile =>
                {
                    LocalTrackFile trackFile = null;
                    Task scanTask = null;

                    try
                    {
                        
                        scanTask = new Task(() =>
                        {
                            trackFile = CreateLocalTrackFromFile(
                                musicStorageFile);
                            AddLocalTrackFileToLibrary(trackFile);
                        });

                        scanTask.Start();
                        scanTask.Wait();
                    }
                    finally
                    {
                        // Report on our progress.
                            Interlocked.Increment(ref FilesProcessed);
                            LastSongAdded = String.Format("{0} by {1}",
                                trackFile.Title, trackFile.AlbumArtist);                        
                    }

                });
            }
            catch (OperationCanceledException scanException)
            {
                CurrentScanState = ScanStates.Cancelled;
                return MusicLibrary;
            }

            CurrentScanState = ScanStates.Idle;

            var serialiseSW = Stopwatch.StartNew();
            DataContractSerializer dcs = new DataContractSerializer(
                typeof(Library)
            );
            MemoryStream libMS = new MemoryStream();
            dcs.WriteObject(libMS, MusicLibrary);
            serialiseSW.Stop();

            return MusicLibrary;
        }

        /// <summary>
        /// Retrieves a list of all files (along with their full paths) in a
        /// given folder.
        /// </summary>
        /// <param name="musicFolder">
        /// The folder in which to search for files.
        /// </param>
        /// <returns></returns>
        protected async Task<ConcurrentQueue<StorageFile>> GetMusicFiles(
            StorageFolder musicFolder)
        {
            var storageFiles = new ConcurrentQueue<StorageFile>();
            var musicFiles =
                await musicFolder.GetFilesAsync(CommonFileQuery.OrderByName);

            var currentStep = 0;
            var totalSteps = musicFiles.Count;

            Parallel.ForEach(musicFiles, musicFile =>
            {
                try
                {
                    storageFiles.Enqueue(musicFile);
                }
                finally
                {
                    Interlocked.Increment(
                        ref currentStep
                    );
                }
            });

            return storageFiles;
        }

        /// <summary>
        /// Creates a new Track object from a music file that has been found on
        /// the storage medium. This file may be qualified by an optional
        /// platform-specific method in order to assess the availability of the
        /// file (such as with OneDrive placeholder files).
        /// </summary>
        /// <param name="musicFile">The file that needs to be initialised as a
        /// track object for the music library.</param>
        /// <param name="fileQualifier">A method that will assess the
        /// eligibility of this file as a Track in the library.</param>
        /// <returns></returns>
        protected LocalTrackFile CreateLocalTrackFromFile(StorageFile musicFile,
            PlatformFileQualifier fileQualifier = null) 
        {
            if (fileQualifier != null)
                if (!fileQualifier(musicFile))
                    return null;

            var propertiesTask = 
                musicFile.Properties.GetMusicPropertiesAsync();

            MusicProperties musicProperties = propertiesTask.AsTask().Result;

            LocalTrackFile localTrackFile = new LocalTrackFile()
            {
                Number = musicProperties.TrackNumber,
                Title = musicProperties.Title,
                Duration = musicProperties.Duration,
                Album = musicProperties.Album,
                AlbumArtist = musicProperties.AlbumArtist,
                MusicStorageFile = musicFile,
                Genres = musicProperties.Genre
            };

            return localTrackFile;
        }

        /// <summary>
        /// Adds a given track to the library being built. This method will
        /// determine the existence of an object in the library before building
        /// the object relationships.
        /// </summary>
        /// <param name="track">The track object that has been initialised and
        /// furnished with basic information.</param>
        /// <param name="albumName">The name of the album to which the track
        /// belongs.</param>
        /// <param name="albumArtistName">The name of the album artist</param>
        protected void AddLocalTrackFileToLibrary(
            LocalTrackFile localTrackFile)
        {
           lock (MusicLibrary)
           {
               string albumName = localTrackFile.Album;
               uint albumYear = localTrackFile.Year;
               string albumArtistName = localTrackFile.AlbumArtist;
               List<string> genres = new List<string>(localTrackFile.Genres);

               Track track = new Track()
               {
                   Title = localTrackFile.Title,
                   Duration = localTrackFile.Duration,
                   Number = localTrackFile.Number,
                   MusicStorageFile = localTrackFile.MusicStorageFile,
                   Year = albumYear
               };
               Album trackAlbum;
               if (!MusicLibrary.Albums.TryGetValue(albumName, out trackAlbum)) {
                   // Create our album
                   trackAlbum = new Album()
                   {
                       Name = albumName,
                       Year = albumYear
                   };
                   // Add it to the music library
                   MusicLibrary.Albums.Add(albumName, trackAlbum);
               }
               // Assign the album to the track
               track.Album = trackAlbum;
               // Add the track to the album
               trackAlbum.Tracks.Add(track);

               foreach (string genre in genres)
               {
                   Genre albumGenre;
                   if (!MusicLibrary.Genres.TryGetValue(genre, out albumGenre))
                   {
                       // Create our genre
                       albumGenre = new Genre() { Name = genre };
                       // Add it to the music library
                       MusicLibrary.Genres.Add(genre, albumGenre);
                   }
                   
                   if (!albumGenre.Albums.ContainsKey(genre))
                       albumGenre.Albums.Add(genre, trackAlbum);

                   // Add the Genre to the Album
                   Genre albumGenreExisting = trackAlbum.Genres.Find(
                       findGenre => findGenre.Name == genre);
                   if (albumGenreExisting == null)
                   {
                       trackAlbum.Genres.Add(albumGenre);
                   }
               }


               AlbumArtist trackAlbumArtist;
               if (!MusicLibrary.AlbumArtists.TryGetValue(albumArtistName, out trackAlbumArtist))
               {
                   // Create our artist
                   trackAlbumArtist = new AlbumArtist() { Name = albumArtistName };
                   // Add it to the music library
                   MusicLibrary.AlbumArtists.Add(albumArtistName, trackAlbumArtist);
               }
               // Assign the artist to the track
               track.AlbumArtist = trackAlbumArtist;
               // Assign the artist to the album
               if (trackAlbum.Artist == null)
                   trackAlbum.Artist = trackAlbumArtist;
               
               if (!trackAlbumArtist.Albums.Contains(trackAlbum))
                   trackAlbumArtist.Albums.Add(trackAlbum);

               // Get the thumbnail stream
               if (trackAlbum.AlbumArt == null)
               {
                    var thumbTask = localTrackFile.MusicStorageFile
                        .GetThumbnailAsync(ThumbnailMode.MusicView,
                        ThumbSize,
                        ThumbnailOptions.ResizeThumbnail).AsTask();
                   
                   thumbTask.Wait();
                   
                   var thumbStream = thumbTask.Result.AsStream();
                   using (MemoryStream thumbMemoryStream = new MemoryStream())
                   {
                       thumbStream.CopyTo(thumbMemoryStream);
                       trackAlbum.AlbumArt = thumbMemoryStream.ToArray();
                   } 
                   
               }
               
               // Finally, add our track to the library
               MusicLibrary.Tracks.Add(track);
           }

        }
    }

    
}