﻿using MusicLibrary.LibraryItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace MusicLibraryBuilder.FileConcepts
{
    /// <summary>
    /// The LocalTrackFile object is similiar to the track object however in
    /// place of referential objects where the album, album artist and genre
    /// are concerned we have primitive objects that represent this data.
    /// This allows a MusicLibrary factory to process this atomically.
    /// </summary>
    public class LocalTrackFile : Track
    {
        
        public new string Album { get; set; }
        public new string AlbumArtist { get; set; }

        public IList<string> Genres { get; set; }

    }
}
