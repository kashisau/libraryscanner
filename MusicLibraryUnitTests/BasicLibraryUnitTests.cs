﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using MusicLibrary;
using MusicLibrary.LibraryItems;

namespace MusicLibraryUnitTests
{
    [TestClass]
    public class BasicLibraryUnitTests
    {
        protected Library MusicLibrary;

        [TestMethod]
        public void LibraryInitialisation()
        {
            MusicLibrary = new Library();
            Assert.IsInstanceOfType(MusicLibrary, typeof(Library));
        }

        [TestMethod]
        public void AddAlbums()
        {
            MusicLibrary = new Library();
            int albumsToCreate = 1000;
            PopulateLibrary(albumsToCreate);

            Assert.AreEqual(MusicLibrary.Albums.Count, albumsToCreate);
        }

        /// <summary>
        /// Populates the music library with albums, each with their own unique artist and tracks. This may be useful for testing.
        /// </summary>
        /// <param name="numberOfAlbums">Optional number of albums to create in the library.</param>
        protected void PopulateLibrary(int numberOfAlbums = 5)
        {
            Genre genre = new Genre();
            genre.Name = "Compilations";

            string[] firstnames = { "Allan", "Peter", "Paul", "Mary", "Henry", "Sally", "Linda", "Mort" };
            string[] lastnames = { "Peterson", "Anderson", "O'Doyle", "Rumin", "Hackett", "Willaims", "Soma" };

            int startingYear = 1999;

            Random random = new Random();

            for (int albumNumber = 0; albumNumber < numberOfAlbums; albumNumber++)
            {
                // Create our new album
                Album newAlbum = new Album();
                string albumName = String.Format("Album {0}", albumNumber);
                newAlbum.Name = albumName;
                newAlbum.Year = (uint)(startingYear + albumNumber);
                newAlbum.Genres.Add(genre);

                // Create our new artist
                int firstnameIndex = random.Next(0, firstnames.Length);
                int lastnameIndex = random.Next(0, lastnames.Length);
                string artistName = String.Format("{0} {1}", firstnames[firstnameIndex], lastnames[lastnameIndex]);

                AlbumArtist newAlbumArtist = null;
                try
                {
                    newAlbumArtist = MusicLibrary.AlbumArtists[artistName];
                }
                catch (KeyNotFoundException)
                {
                    newAlbumArtist = new AlbumArtist();
                    newAlbumArtist.Name = artistName;
                    newAlbumArtist.Albums.Add(newAlbum);
                    MusicLibrary.AlbumArtists.Add(newAlbumArtist.Name, newAlbumArtist);
                }

                newAlbum.Artist = newAlbumArtist;
                newAlbumArtist.Albums.Add(newAlbum);

                // Add some tracks
                for (int i = 0; i < random.Next(9, 18); i++)
                {
                    Track newTrack = CreateNewTrack(newAlbum);
                    MusicLibrary.Tracks.Add(newTrack);
                }

                // Add it to the library
                MusicLibrary.Albums.Add(newAlbum.Name, newAlbum);

            }
        }

        /// <summary>
        /// Creates a new Track based on the album supplied. The supplied album is attributed to the track and the
        /// album's album artist property is used for the track's artist. The track number is also based on the number
        /// of tracks that already exist in the album.
        /// 
        /// Once the track is created it is then added to the list of tracks in the album supplied.
        /// </summary>
        /// <param name="trackAlbum"></param>
        /// <returns></returns>
        protected Track CreateNewTrack(Album trackAlbum)
        {
            int trackNumber = trackAlbum.Tracks.Count;
            string trackTitle = String.Format("Track {0}", trackNumber);

            Random random = new Random();
            AlbumArtist trackArtist = trackAlbum.Artist;
            string trackFilepath = String.Format(
                "{0}/{1}/{2} - {3}.mp4",
                trackArtist.Name,
                trackAlbum.Name,
                trackNumber,
                trackTitle
            );
            Track newTrack = new Track();

            newTrack.Album = trackAlbum;
            newTrack.AlbumArtist = trackArtist;
            newTrack.Number = (uint)(trackNumber + 1);
            newTrack.Title = trackTitle;
            newTrack.Duration = new TimeSpan(random.Next(1,5), random.Next(0,59), random.Next(0,59), random.Next(0,999));

            trackAlbum.Tracks.Add(newTrack);

            return newTrack;
        }
    }
}
